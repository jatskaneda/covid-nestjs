"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../entity/user.entity");
const typeorm_2 = require("typeorm");
const user_dto_1 = require("../dto/user.dto");
const argon2 = require("argon2");
const moment = require("moment");
const config_1 = require("../../config");
const jwt = require("jsonwebtoken");
let UserService = class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async findOneById(id) {
        return await this.userRepository
            .createQueryBuilder("user")
            .where("user.id = :id", { id })
            .getOne();
    }
    async findOneBycredentials({ cedula, password, }) {
        const user = await this.userRepository
            .createQueryBuilder("user")
            .innerJoinAndSelect("user.role", "role")
            .where("cedula = :cedula", { cedula })
            .getOne();
        if (!user)
            return null;
        if (await argon2.verify(user.password, password))
            return user;
        return null;
    }
    generateJWT(user) {
        return jwt.sign({
            id: user.id,
            exp: moment()
                .add(1, "h")
                .unix(),
        }, config_1.SECRET);
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map