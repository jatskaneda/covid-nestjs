import { UserEntity } from "../entity/user.entity";
import { Repository } from "typeorm";
import { LoginUserDTO } from "src/dto/user.dto";
export declare class UserService {
    private userRepository;
    constructor(userRepository: Repository<UserEntity>);
    findOneById(id: string): Promise<UserEntity>;
    findOneBycredentials({ cedula, password, }: LoginUserDTO): Promise<UserEntity>;
    generateJWT(user: UserEntity): string;
}
