import { CovidEntity } from "../entity/covid.entity";
import { Repository } from "typeorm";
export declare class CovidService {
    private covidRepository;
    constructor(covidRepository: Repository<CovidEntity>);
    buscarTodo(): Promise<CovidEntity[]>;
    createAndSave(datos: CovidEntity): Promise<void>;
}
