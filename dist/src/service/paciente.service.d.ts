import { PacienteEntity } from "../entity/paciente.entity";
import { Repository } from "typeorm";
export declare class PacienteService {
    private pacienteRepository;
    constructor(pacienteRepository: Repository<PacienteEntity>);
    buscarTodo(): Promise<PacienteEntity[]>;
    buscarDetalle(pacienteId: string): Promise<any>;
    createAndSave(datos: PacienteEntity): Promise<void>;
}
