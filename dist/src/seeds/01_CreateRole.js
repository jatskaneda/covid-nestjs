"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const role_entity_1 = require("../entity/role.entity");
const generic_constants_1 = require("../constants/generic.constants");
class CreateRole {
    async run(factory, connection) {
        await connection
            .createQueryBuilder()
            .insert()
            .into(role_entity_1.UserRoleEntity)
            .values([
            { name: generic_constants_1.UserRoleEnum.ADMIN },
            { name: generic_constants_1.UserRoleEnum.ADVANCED },
            { name: generic_constants_1.UserRoleEnum.REGULAR },
        ])
            .execute();
    }
}
exports.default = CreateRole;
//# sourceMappingURL=01_CreateRole.js.map