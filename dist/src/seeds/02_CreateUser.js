"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_entity_1 = require("../entity/user.entity");
const role_entity_1 = require("../entity/role.entity");
const generic_constants_1 = require("../constants/generic.constants");
const argon2 = require("argon2");
class CreateUser {
    async run(factory, connection) {
        const adminRole = await connection
            .getRepository(role_entity_1.UserRoleEntity)
            .findOne({ name: generic_constants_1.UserRoleEnum.ADMIN });
        await connection
            .createQueryBuilder()
            .insert()
            .into(user_entity_1.UserEntity)
            .values([
            {
                tipoCedula: generic_constants_1.TipoCedulaEnum.VENEZOLANO,
                nombre: "Jonathan",
                apellido: "Tovar",
                cedula: 14261995,
                password: await argon2.hash("1234"),
                isActive: true,
                role: adminRole,
            },
        ])
            .execute();
    }
}
exports.default = CreateUser;
//# sourceMappingURL=02_CreateUser.js.map