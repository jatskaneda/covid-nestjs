import { UserRoleEntity } from "./role.entity";
import { PacienteEntity } from "./paciente.entity";
import { CovidEntity } from "./covid.entity";
export declare class UserEntity {
    id: string;
    tipoCedula: string;
    cedula: number;
    nombre: string;
    apellido: string;
    password: string;
    email: string;
    question: string;
    reply: string;
    isActive: boolean;
    creadoPor: string;
    createdAt: Date;
    updatedAt: Date;
    paciente: PacienteEntity[];
    role: UserRoleEntity;
    covid: CovidEntity[];
    hashPassword(): Promise<void>;
}
