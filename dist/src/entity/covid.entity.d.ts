import { PacienteEntity } from "./paciente.entity";
import { UserEntity } from "./user.entity";
export declare class CovidEntity {
    id: string;
    tipoPrueba: string;
    fechaPrueba: Date;
    resultado: boolean;
    creadoPor: UserEntity;
    createdAt: Date;
    updatedAt: Date;
    paciente: PacienteEntity;
}
