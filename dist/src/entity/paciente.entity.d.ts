import { CovidEntity } from "./covid.entity";
import { UserEntity } from "./user.entity";
export declare class PacienteEntity {
    id: string;
    tipoCedula: string;
    cedula: number;
    nombre: string;
    apellido: string;
    sexo: string;
    edad: string;
    unidad: string;
    parroquia: string;
    telefono: string;
    email: string;
    creadoPor: UserEntity;
    createdAt: Date;
    updatedAt: Date;
    covid: CovidEntity[];
}
