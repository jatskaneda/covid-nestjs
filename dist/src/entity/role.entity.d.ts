import { UserEntity } from "./user.entity";
export declare class UserRoleEntity {
    id: string;
    name: string;
    user: UserEntity[];
}
