"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PacienteEntity = void 0;
const typeorm_1 = require("typeorm");
const covid_entity_1 = require("./covid.entity");
const user_entity_1 = require("./user.entity");
let PacienteEntity = class PacienteEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn("uuid"),
    __metadata("design:type", String)
], PacienteEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], PacienteEntity.prototype, "tipoCedula", void 0);
__decorate([
    typeorm_1.Column({ type: "integer" }),
    __metadata("design:type", Number)
], PacienteEntity.prototype, "cedula", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], PacienteEntity.prototype, "nombre", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], PacienteEntity.prototype, "apellido", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], PacienteEntity.prototype, "sexo", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], PacienteEntity.prototype, "edad", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], PacienteEntity.prototype, "unidad", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], PacienteEntity.prototype, "parroquia", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], PacienteEntity.prototype, "telefono", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], PacienteEntity.prototype, "email", void 0);
__decorate([
    typeorm_1.ManyToOne(() => user_entity_1.UserEntity, (userEntity) => userEntity.paciente),
    typeorm_1.JoinColumn(),
    __metadata("design:type", user_entity_1.UserEntity)
], PacienteEntity.prototype, "creadoPor", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ type: "timestamp" }),
    __metadata("design:type", Date)
], PacienteEntity.prototype, "createdAt", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ type: "timestamp" }),
    __metadata("design:type", Date)
], PacienteEntity.prototype, "updatedAt", void 0);
__decorate([
    typeorm_1.OneToMany(() => covid_entity_1.CovidEntity, (covidEntity) => covidEntity.paciente),
    typeorm_1.JoinColumn(),
    __metadata("design:type", Array)
], PacienteEntity.prototype, "covid", void 0);
PacienteEntity = __decorate([
    typeorm_1.Entity({ name: "paciente" })
], PacienteEntity);
exports.PacienteEntity = PacienteEntity;
//# sourceMappingURL=paciente.entity.js.map