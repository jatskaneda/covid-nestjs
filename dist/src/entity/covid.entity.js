"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CovidEntity = void 0;
const typeorm_1 = require("typeorm");
const paciente_entity_1 = require("./paciente.entity");
const user_entity_1 = require("./user.entity");
let CovidEntity = class CovidEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn("uuid"),
    __metadata("design:type", String)
], CovidEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], CovidEntity.prototype, "tipoPrueba", void 0);
__decorate([
    typeorm_1.Column({ type: "date" }),
    __metadata("design:type", Date)
], CovidEntity.prototype, "fechaPrueba", void 0);
__decorate([
    typeorm_1.Column({ type: "boolean" }),
    __metadata("design:type", Boolean)
], CovidEntity.prototype, "resultado", void 0);
__decorate([
    typeorm_1.ManyToOne(() => user_entity_1.UserEntity, (userEntity) => userEntity.covid),
    typeorm_1.JoinColumn(),
    __metadata("design:type", user_entity_1.UserEntity)
], CovidEntity.prototype, "creadoPor", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ type: "timestamp" }),
    __metadata("design:type", Date)
], CovidEntity.prototype, "createdAt", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ type: "timestamp" }),
    __metadata("design:type", Date)
], CovidEntity.prototype, "updatedAt", void 0);
__decorate([
    typeorm_1.ManyToOne(() => paciente_entity_1.PacienteEntity, (pacienteEntity) => pacienteEntity.covid),
    typeorm_1.JoinColumn(),
    __metadata("design:type", paciente_entity_1.PacienteEntity)
], CovidEntity.prototype, "paciente", void 0);
CovidEntity = __decorate([
    typeorm_1.Entity({ name: "covid" })
], CovidEntity);
exports.CovidEntity = CovidEntity;
//# sourceMappingURL=covid.entity.js.map