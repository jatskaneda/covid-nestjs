export interface UserLoginIN {
    nombre: string;
    apellido: string;
    role: string;
    token: string;
}
