"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoCedulaEnum = exports.UserRoleEnum = void 0;
var UserRoleEnum;
(function (UserRoleEnum) {
    UserRoleEnum["ADMIN"] = "administrador del sistema";
    UserRoleEnum["ADVANCED"] = "usuario avanzado";
    UserRoleEnum["REGULAR"] = "usuario estandar";
})(UserRoleEnum = exports.UserRoleEnum || (exports.UserRoleEnum = {}));
var TipoCedulaEnum;
(function (TipoCedulaEnum) {
    TipoCedulaEnum["VENEZOLANO"] = "V";
    TipoCedulaEnum["EXTRANJERO"] = "E";
})(TipoCedulaEnum = exports.TipoCedulaEnum || (exports.TipoCedulaEnum = {}));
//# sourceMappingURL=generic.constants.js.map