export declare enum UserRoleEnum {
    ADMIN = "administrador del sistema",
    ADVANCED = "usuario avanzado",
    REGULAR = "usuario estandar"
}
export declare enum TipoCedulaEnum {
    VENEZOLANO = "V",
    EXTRANJERO = "E"
}
