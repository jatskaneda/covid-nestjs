"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CovidController = void 0;
const common_1 = require("@nestjs/common");
const covid_service_1 = require("../service/covid.service");
const covid_entity_1 = require("../entity/covid.entity");
let CovidController = class CovidController {
    constructor(covidService) {
        this.covidService = covidService;
    }
    async obtenerCovid() {
        return await this.covidService.buscarTodo();
    }
    async insertarCovid(covidData) {
        return await this.covidService.createAndSave(covidData);
    }
    async updateCovid(covidData) {
        return await this.covidService.createAndSave(covidData);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CovidController.prototype, "obtenerCovid", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [covid_entity_1.CovidEntity]),
    __metadata("design:returntype", Promise)
], CovidController.prototype, "insertarCovid", null);
__decorate([
    common_1.Patch(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [covid_entity_1.CovidEntity]),
    __metadata("design:returntype", Promise)
], CovidController.prototype, "updateCovid", null);
CovidController = __decorate([
    common_1.Controller("covid"),
    __metadata("design:paramtypes", [covid_service_1.CovidService])
], CovidController);
exports.CovidController = CovidController;
//# sourceMappingURL=covid.controller.js.map