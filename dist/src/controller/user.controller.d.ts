import { UserService } from "../service/user.service";
import { UserLoginIN } from "../interface/user.interface";
import { LoginUserDTO } from "../dto/user.dto";
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    login(loginUserData: LoginUserDTO): Promise<UserLoginIN>;
}
