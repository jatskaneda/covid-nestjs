import { CovidService } from "src/service/covid.service";
import { CovidEntity } from "src/entity/covid.entity";
export declare class CovidController {
    private readonly covidService;
    constructor(covidService: CovidService);
    obtenerCovid(): Promise<CovidEntity[]>;
    insertarCovid(covidData: CovidEntity): Promise<void>;
    updateCovid(covidData: CovidEntity): Promise<void>;
}
