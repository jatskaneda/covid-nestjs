import { PacienteService } from "src/service/paciente.service";
import { PacienteEntity } from "src/entity/paciente.entity";
export declare class PacienteController {
    private readonly pacienteService;
    constructor(pacienteService: PacienteService);
    obtenerPaciente(): Promise<PacienteEntity[]>;
    obtenerPacienteId(params: any): Promise<any>;
    insertarPaciente(pacienteData: PacienteEntity): Promise<void>;
    updatePaciente(pacienteData: PacienteEntity): Promise<void>;
}
