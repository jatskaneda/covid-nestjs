"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PacienteController = void 0;
const common_1 = require("@nestjs/common");
const paciente_service_1 = require("../service/paciente.service");
const paciente_entity_1 = require("../entity/paciente.entity");
let PacienteController = class PacienteController {
    constructor(pacienteService) {
        this.pacienteService = pacienteService;
    }
    async obtenerPaciente() {
        return await this.pacienteService.buscarTodo();
    }
    async obtenerPacienteId(params) {
        return await this.pacienteService.buscarDetalle(params.id);
    }
    async insertarPaciente(pacienteData) {
        return await this.pacienteService.createAndSave(pacienteData);
    }
    async updatePaciente(pacienteData) {
        return await this.pacienteService.createAndSave(pacienteData);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "obtenerPaciente", null);
__decorate([
    common_1.Get(":id"),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "obtenerPacienteId", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [paciente_entity_1.PacienteEntity]),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "insertarPaciente", null);
__decorate([
    common_1.Patch(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [paciente_entity_1.PacienteEntity]),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "updatePaciente", null);
PacienteController = __decorate([
    common_1.Controller("paciente"),
    __metadata("design:paramtypes", [paciente_service_1.PacienteService])
], PacienteController);
exports.PacienteController = PacienteController;
//# sourceMappingURL=paciente.controller.js.map