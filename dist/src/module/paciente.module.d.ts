import { MiddlewareConsumer, NestModule } from "@nestjs/common";
export declare class PacienteModule implements NestModule {
    configure(consumer: MiddlewareConsumer): void;
}
