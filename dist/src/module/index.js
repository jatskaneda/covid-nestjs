"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_module_1 = require("./user.module");
const paciente_module_1 = require("./paciente.module");
const covid_module_1 = require("./covid.module");
exports.default = [user_module_1.UserModule, paciente_module_1.PacienteModule, covid_module_1.CovidModule];
//# sourceMappingURL=index.js.map