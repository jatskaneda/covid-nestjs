import { MiddlewareConsumer, NestModule } from "@nestjs/common";
export declare class CovidModule implements NestModule {
    configure(consumer: MiddlewareConsumer): void;
}
