"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PacienteModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../entity/user.entity");
const paciente_entity_1 = require("../entity/paciente.entity");
const auth_middleware_1 = require("../middleware/auth.middleware");
const user_service_1 = require("../service/user.service");
const user_module_1 = require("./user.module");
const paciente_controller_1 = require("../controller/paciente.controller");
const paciente_service_1 = require("../service/paciente.service");
let PacienteModule = class PacienteModule {
    configure(consumer) {
        consumer
            .apply(auth_middleware_1.AuthMiddleware)
            .forRoutes({ path: "paciente", method: common_1.RequestMethod.ALL });
    }
};
PacienteModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([user_entity_1.UserEntity, paciente_entity_1.PacienteEntity]), user_module_1.UserModule],
        providers: [user_service_1.UserService, paciente_service_1.PacienteService],
        controllers: [paciente_controller_1.PacienteController],
    })
], PacienteModule);
exports.PacienteModule = PacienteModule;
//# sourceMappingURL=paciente.module.js.map