import { IsNotEmpty } from "class-validator";

export class LoginUserDTO {
  @IsNotEmpty()
  readonly cedula: number;

  @IsNotEmpty()
  readonly password: string;
}
