import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
} from "typeorm";
import { PacienteEntity } from "./paciente.entity";
import { UserEntity } from "./user.entity";

@Entity({ name: "covid" })
export class CovidEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  tipoPrueba: string;

  @Column({ type: "date" })
  fechaPrueba: Date;

  @Column({ type: "boolean" })
  resultado: boolean;

  @ManyToOne(
    () => UserEntity,
    (userEntity) => userEntity.covid
  )
  @JoinColumn()
  creadoPor: UserEntity;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;

  @ManyToOne(
    () => PacienteEntity,
    (pacienteEntity) => pacienteEntity.covid
  )
  @JoinColumn()
  paciente: PacienteEntity;
}
