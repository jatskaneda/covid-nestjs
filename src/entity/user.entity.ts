import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
} from "typeorm";

import { UserRoleEntity } from "./role.entity";
import * as argon2 from "argon2";
import { PacienteEntity } from "./paciente.entity";
import { CovidEntity } from "./covid.entity";
import { from } from "rxjs";

@Entity({ name: "user" })
export class UserEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  tipoCedula: string;

  @Column({ type: "integer" })
  cedula: number;

  @Column()
  nombre: string;

  @Column()
  apellido: string;

  @Column()
  password: string;

  @Column({ nullable: true })
  email: string;

  @Column({ nullable: true })
  question: string;

  @Column({ nullable: true })
  reply: string;

  @Column({ default: true })
  isActive: boolean;

  @Column({ nullable: true })
  creadoPor: string;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;

  @OneToMany(
    () => PacienteEntity,
    (pacienteEntity) => pacienteEntity.creadoPor
  )
  @JoinColumn()
  paciente: PacienteEntity[];

  @ManyToOne(
    () => UserRoleEntity,
    (roleEntity) => roleEntity.user
  )
  @JoinColumn()
  role: UserRoleEntity;

  @OneToMany(
    () => CovidEntity,
    (covidEntity) => covidEntity.creadoPor
  )
  @JoinColumn()
  covid: CovidEntity[];

  @BeforeInsert()
  async hashPassword() {
    this.password = await argon2.hash(this.password);
  }
}
