import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn } from "typeorm";
import { UserEntity } from "./user.entity";

@Entity({ name: "user_role" })
export class UserRoleEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  name: string;

  @OneToMany(
    () => UserEntity,
    user => user.role,
  )
  @JoinColumn()
  user: UserEntity[];
}
