import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  BeforeInsert,
  DeleteDateColumn,
} from "typeorm";
import { CovidEntity } from "./covid.entity";
import { UserEntity } from "./user.entity";

@Entity({ name: "paciente" })
export class PacienteEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  tipoCedula: string;

  @Column({ type: "integer" })
  cedula: number;

  @Column()
  nombre: string;

  @Column()
  apellido: string;

  @Column()
  sexo: string;

  @Column()
  edad: string;

  @Column()
  unidad: string;

  @Column()
  parroquia: string;

  @Column()
  telefono: string;

  @Column()
  email: string;

  @ManyToOne(
    () => UserEntity,
    (userEntity) => userEntity.paciente
  )
  @JoinColumn()
  creadoPor: UserEntity;

  @CreateDateColumn({ type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt: Date;

  @OneToMany(
    () => CovidEntity,
    (covidEntity) => covidEntity.paciente
  )
  @JoinColumn()
  covid: CovidEntity[];
}
