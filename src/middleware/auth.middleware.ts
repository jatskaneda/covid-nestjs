import { HttpException } from "@nestjs/common/exceptions/http.exception";
import { NestMiddleware, HttpStatus, Injectable } from "@nestjs/common";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import { SECRET } from "../../config";
import { UserService } from "../service/user.service";

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly userService: UserService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    try {
      const authHeaders = req.headers.authorization;
      if (authHeaders && (authHeaders as string).split(" ")[1]) {
        const token = (authHeaders as string).split(" ")[1];

        const decoded: any = jwt.verify(token, SECRET);
        const user = await this.userService.findOneById(decoded.id);
        if (!user) throw new Error();
        next();
      } else {
        throw new Error();
      }
    } catch (error) {
      throw new HttpException("Not authorized", HttpStatus.UNAUTHORIZED);
    }
  }
}
