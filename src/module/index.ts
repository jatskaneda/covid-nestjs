import { UserModule } from "./user.module";
import { PacienteModule } from "./paciente.module";
import { CovidModule } from "./covid.module";

export default [UserModule, PacienteModule, CovidModule];
