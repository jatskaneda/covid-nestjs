import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserEntity } from "../entity/user.entity";
import { PacienteEntity } from "../entity/paciente.entity";
import { AuthMiddleware } from "../middleware/auth.middleware";
import { UserService } from "src/service/user.service";
import { UserModule } from "./user.module";
import { PacienteController } from "src/controller/paciente.controller";
import { PacienteService } from "src/service/paciente.service";

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, PacienteEntity]), UserModule],
  providers: [UserService, PacienteService],
  controllers: [PacienteController],
})
export class PacienteModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({ path: "paciente", method: RequestMethod.ALL });
  }
}
