import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserEntity } from "../entity/user.entity";
import { PacienteEntity } from "../entity/paciente.entity";
import { CovidEntity } from "../entity/covid.entity";
import { AuthMiddleware } from "../middleware/auth.middleware";
import { UserService } from "src/service/user.service";
import { PacienteService } from "src/service/paciente.service";
import { UserModule } from "./user.module";
import { PacienteModule } from "./paciente.module";
import { CovidController } from "src/controller/covid.controller";
import { CovidService } from "src/service/covid.service";

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity, PacienteEntity, CovidEntity]),
    UserModule,
    PacienteModule,
  ],
  providers: [UserService, PacienteService, CovidService],
  controllers: [CovidController],
})
export class CovidModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({ path: "covid", method: RequestMethod.ALL });
  }
}
