import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { UserEntity } from "../entity/user.entity";
import { UserRoleEntity } from "../entity/role.entity";
import { UserRoleEnum, TipoCedulaEnum } from "../constants/generic.constants";
import * as argon2 from "argon2";

export default class CreateUser implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const adminRole = await connection
      .getRepository(UserRoleEntity)
      .findOne({ name: UserRoleEnum.ADMIN });

    await connection
      .createQueryBuilder()
      .insert()
      .into(UserEntity)
      .values([
        {
          tipoCedula: TipoCedulaEnum.VENEZOLANO,
          nombre: "Jonathan",
          apellido: "Tovar",
          cedula: 14261995,
          password: await argon2.hash("1234"),
          isActive: true,
          role: adminRole,
        },
      ])
      .execute();
  }
}
