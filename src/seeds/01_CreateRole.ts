import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { UserRoleEntity } from "../entity/role.entity";
import { UserRoleEnum } from "../constants/generic.constants";

export default class CreateRole implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(UserRoleEntity)
      .values([
        { name: UserRoleEnum.ADMIN },
        { name: UserRoleEnum.ADVANCED },
        { name: UserRoleEnum.REGULAR },
      ])
      .execute();
  }
}
