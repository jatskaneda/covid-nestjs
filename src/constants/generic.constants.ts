export enum UserRoleEnum {
  ADMIN = "administrador del sistema",
  ADVANCED = "usuario avanzado",
  REGULAR = "usuario estandar",
}

export enum TipoCedulaEnum {
  VENEZOLANO = "V",
  EXTRANJERO = "E",
}
