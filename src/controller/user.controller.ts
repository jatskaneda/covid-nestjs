import { Controller, Get, Post, Body } from "@nestjs/common";
import { UserService } from "../service/user.service";
import { UserLoginIN } from "../interface/user.interface";
import { LoginUserDTO } from "../dto/user.dto";
import { HttpException } from "@nestjs/common/exceptions/http.exception";

@Controller("user")
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post("login")
  async login(@Body() loginUserData: LoginUserDTO): Promise<UserLoginIN> {
    const _user = await this.userService.findOneBycredentials(loginUserData);
    const errors = { message: " User not found" };
    if (!_user) throw new HttpException(errors, 401);
    const token = this.userService.generateJWT(_user);

    return {
      nombre: _user.nombre,
      apellido: _user.apellido,
      role: _user.role.name,
      token,
    };
  }
}
