import { Controller, Get, Post, Body, Patch } from "@nestjs/common";
import { CovidService } from "src/service/covid.service";
import { CovidEntity } from "src/entity/covid.entity";

@Controller("covid")
export class CovidController {
  constructor(private readonly covidService: CovidService) {}

  @Get()
  async obtenerCovid(): Promise<CovidEntity[]> {
    return await this.covidService.buscarTodo();
  }

  @Post()
  async insertarCovid(@Body() covidData: CovidEntity): Promise<void> {
    return await this.covidService.createAndSave(covidData);
  }

  @Patch()
  async updateCovid(@Body() covidData: CovidEntity): Promise<void> {
    return await this.covidService.createAndSave(covidData);
  }
}
