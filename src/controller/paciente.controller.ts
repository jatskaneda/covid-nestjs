import { Controller, Get, Post, Body, Patch, Param } from "@nestjs/common";
import { PacienteService } from "src/service/paciente.service";
import { PacienteEntity } from "src/entity/paciente.entity";

@Controller("paciente")
export class PacienteController {
  constructor(private readonly pacienteService: PacienteService) {}

  @Get()
  async obtenerPaciente(): Promise<PacienteEntity[]> {
    return await this.pacienteService.buscarTodo();
  }

  @Get(":id")
  async obtenerPacienteId(@Param() params): Promise<any> {
    return await this.pacienteService.buscarDetalle(params.id);
  }

  @Post()
  async insertarPaciente(@Body() pacienteData: PacienteEntity): Promise<void> {
    return await this.pacienteService.createAndSave(pacienteData);
  }

  @Patch()
  async updatePaciente(@Body() pacienteData: PacienteEntity): Promise<void> {
    return await this.pacienteService.createAndSave(pacienteData);
  }
}
