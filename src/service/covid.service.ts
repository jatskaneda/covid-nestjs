import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CovidEntity } from "../entity/covid.entity";
import { Repository } from "typeorm";
import { POINT_CONVERSION_HYBRID } from "constants";
import { getManager } from "typeorm";

@Injectable()
export class CovidService {
  constructor(
    @InjectRepository(CovidEntity)
    private covidRepository: Repository<CovidEntity>
  ) {}

  async buscarTodo(): Promise<CovidEntity[]> {
    return await this.covidRepository.createQueryBuilder("covid").getMany();
  }

  async createAndSave(datos: CovidEntity): Promise<void> {
    const covid = new CovidEntity();
    covid.id = datos.id || undefined;
    covid.fechaPrueba = datos.fechaPrueba;
    covid.tipoPrueba = datos.tipoPrueba;
    covid.resultado = datos.resultado;
    covid.creadoPor = datos.creadoPor;
    covid.paciente = datos.paciente;
    this.covidRepository.save(covid);
  }
}
