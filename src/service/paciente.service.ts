import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { PacienteEntity } from "../entity/paciente.entity";
import { Repository } from "typeorm";

@Injectable()
export class PacienteService {
  constructor(
    @InjectRepository(PacienteEntity)
    private pacienteRepository: Repository<PacienteEntity>
  ) {}

  async buscarTodo(): Promise<PacienteEntity[]> {
    return await this.pacienteRepository
      .createQueryBuilder("paciente")
      .getMany();
  }

  async buscarDetalle(pacienteId: string): Promise<any> {
    return await this.pacienteRepository
      .createQueryBuilder("paciente")
      .leftJoinAndSelect("paciente.covid", "covid")
      .where("paciente.id = :pacienteId", { pacienteId })
      .getOne();
  }

  async createAndSave(datos: PacienteEntity): Promise<void> {
    const paciente = new PacienteEntity();
    paciente.id = datos.id || undefined;
    paciente.cedula = datos.cedula;
    paciente.nombre = datos.nombre;
    paciente.apellido = datos.apellido;
    paciente.sexo = datos.sexo;
    paciente.edad = datos.edad;
    paciente.unidad = datos.unidad;
    paciente.parroquia = datos.parroquia;
    paciente.telefono = datos.telefono;
    paciente.email = datos.email;
    paciente.creadoPor = datos.creadoPor;
    this.pacienteRepository.save(paciente);
  }
}
