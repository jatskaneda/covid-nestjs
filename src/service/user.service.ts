import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { HttpException } from "@nestjs/common/exceptions/http.exception";
import { UserEntity } from "../entity/user.entity";
import { Repository } from "typeorm";
import { LoginUserDTO } from "src/dto/user.dto";
import * as argon2 from "argon2";
import * as moment from "moment";
import { SECRET } from "../../config";
import * as jwt from "jsonwebtoken";

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>
  ) {}

  async findOneById(id: string): Promise<UserEntity> {
    return await this.userRepository
      .createQueryBuilder("user")
      .where("user.id = :id", { id })
      .getOne();
  }

  async findOneBycredentials({
    cedula,
    password,
  }: LoginUserDTO): Promise<UserEntity> {
    const user = await this.userRepository
      .createQueryBuilder("user")
      .innerJoinAndSelect("user.role", "role")
      .where("cedula = :cedula", { cedula })
      .getOne();

    if (!user) return null;
    if (await argon2.verify(user.password, password)) return user;
    return null;
  }

  generateJWT(user: UserEntity): string {
    return jwt.sign(
      {
        id: user.id,
        exp: moment()
          .add(1, "h")
          .unix(),
      },
      SECRET
    );
  }
}
