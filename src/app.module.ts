import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import ProjectModules from "./module";

@Module({
  imports: [TypeOrmModule.forRoot(), ...ProjectModules],
})
export class AppModule {}
